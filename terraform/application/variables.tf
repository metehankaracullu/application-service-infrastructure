data "aws_vpc" "selected" {
  filter {
    name = "tag:Name"
    values = [
      var.vpc_name]
  }
}
variable "vpc_id" {}
variable "vpc_cidr" {}
variable "vpc_name" {}
variable "private_1a" {}
variable "private_1b" {}
variable "private_1c" {}
variable "account_id" {}
variable "environment" {}
variable "task_cpu" {}
variable "task_memory" {}
variable "hosted_zone_id" {}
variable "config_server_host" {}
variable "desired_task_number" {}
variable "region" {
  default = "eu-central-1"
}
variable "service_name" {
  default = "application-service"
}
variable "lb_prefix" {
  default = "application-application"
}
variable "ecs_cluster_name" {
  default = "application-service"
}
variable "container_port" {
  default = 8080
}
variable "health_check_path" {
  default = "/actuator/health"
}
variable "lb_logs_bucket_name" {
  default = "application-load-balancer-logs"
}
variable "services_and_capacities" {
  type = map(object({
    min   = number
    max   = number
    name  = string
  }))
}