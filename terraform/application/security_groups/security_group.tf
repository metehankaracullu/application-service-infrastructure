resource "aws_security_group" "application_service_lb_sg" {
  name              = "${var.lb_security_group_name}-${var.environment}"
  vpc_id            = var.vpc_id

  ingress {
    protocol        = "tcp"
    from_port       = 80
    to_port         = 80
    cidr_blocks     = [
      var.vpc_cidr]
    description     = "${var.environment}-VPC-cidr-blocks"
  }

  egress {
    protocol        = "-1"
    from_port       = 0
    to_port         = 0
    cidr_blocks     = [
      "0.0.0.0/0"]
  }
  tags = merge(
    local.common_tags,
    map(
      "Name", "application Service LB SG"
    )
  )
}

resource "aws_security_group" "application_service_task_sg" {
  name              = "${var.task_security_group_name}-${var.environment}"
  vpc_id            = var.vpc_id

  ingress {
    protocol        = "tcp"
    from_port       = 8080
    to_port         = 8080
    cidr_blocks     = [
      var.vpc_cidr]
    description     = "${var.environment}-VPC-cidr-blocks"
  }

  ingress {
    protocol        = "tcp"
    from_port       = 0
    to_port         = 65535
    security_groups = [
      aws_security_group.application_service_lb_sg.id]
    description     = "application_service_lb_sg"
  }

  egress {
    protocol        = "-1"
    from_port       = 0
    to_port         = 0
    cidr_blocks     = [
      "0.0.0.0/0"]
  }
  tags = merge(
    local.common_tags,
    map(
      "Name", "application Service Task SG"
    )
  )
}

locals {
  common_tags = {
    ManagedBy = "terraform"
    Team = "Team"
    Project = "application-service-infrastructure"
    Domain = "application"
    Component = "application-service-application"
    Stage = var.environment
  }
}