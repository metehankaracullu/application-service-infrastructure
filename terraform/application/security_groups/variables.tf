variable "vpc_cidr" {}
variable "vpc_id" {}
variable "environment" {}
variable "lb_security_group_name" {
  default = "application-service-load-balancer-sg"
}
variable "task_security_group_name" {
  default = "application-service-tasks-sg"
}