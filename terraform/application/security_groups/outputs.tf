output "task_sg_id" {
  value = aws_security_group.application_service_task_sg.id
}
output "lb_sg_id" {
  value = aws_security_group.application_service_lb_sg.id
}