# TASK DEFINITION
resource "aws_ecs_task_definition" "task_definition" {
  count                     = length(var.services)
  network_mode              = "awsvpc"
  requires_compatibilities  = ["FARGATE"]
  family                    = "${var.service_name}-${var.services[count.index]}-${var.environment}"
  cpu                       = var.task_cpu
  memory                    = var.task_memory
  container_definitions     = data.template_file.task_definition_json[count.index].rendered
  execution_role_arn        = aws_iam_role.task_execution_role.arn
  task_role_arn             = aws_iam_role.task_role.arn
  depends_on                = [data.template_file.task_definition_json]

  tags = merge(
    local.common_tags,
    map(
      "Name", "application Service Task Definitions"
    )
  )
}

data "template_file" "task_definition_json" {
  count                           = length(var.services)
  template                        = file("${path.module}/task_definition.json")
  vars = {
    image_url                     = "${module.ecr.ecr_repository_url}:${var.services[count.index]}"
    service_name                  = var.service_name
    container_port                = var.container_port
    memory                        = var.task_memory
    task_cpu                      = var.task_cpu
    environment                   = var.environment
    region                        = var.region
    account_id                    = var.account_id
    config_server_host            = var.config_server_host
    module_name                   = var.services[count.index]
  }
}

resource "aws_iam_role" "task_execution_role" {
  name                = "${var.service_name}-task_execution_role-${var.environment}"
  assume_role_policy  = file("${path.module}/iam_policies/task_execution_role.json")
  tags = merge(
  local.common_tags,
  map(
  "Name", "application Service Task Execution Role"
  )
  )
}

resource "aws_iam_role" "task_role" {
  name                = "${var.service_name}-task-role-${var.environment}"
  assume_role_policy  = file("${path.module}/iam_policies/task_role.json")
  tags = merge(
  local.common_tags,
  map(
  "Name", "application Service task role"
  )
  )
}

resource "aws_iam_role_policy" "task_execution_policy" {
  name    = "${var.service_name}-task_execution_policy"
  role    = aws_iam_role.task_execution_role.id
  policy  = data.template_file.task_execution_policy_file.rendered
  depends_on = [data.template_file.task_execution_policy_file]
}

data "template_file" "task_execution_policy_file" {
  template = file("${path.module}/iam_policies/task_execution_policy.json")
  vars = {
    region      = var.region
    account_id  = var.account_id
  }
}

module "ecr" {
  source            = "../ecr"
  environment    = var.environment
}

locals {
  common_tags = {
    ManagedBy = "terraform"
    Team = "Team"
    Project = "application-service-infrastructure"
    Domain = "application"
    Component = "application-service-infrastructure"
    Stage = var.environment
  }
}