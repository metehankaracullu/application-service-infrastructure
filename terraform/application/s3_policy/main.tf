resource "aws_s3_bucket_policy" "log_bucket_policy" {
  bucket = var.log_bucket_name
  policy = data.template_file.s3_policy_file.rendered
  depends_on = [data.template_file.s3_policy_file]
}

data "template_file" "s3_policy_file" {
  template = file("${path.module}/policy.json")
  vars = {
    elb_service_account_arn = var.elb_service_account_arn
    log_bucket_name         = var.log_bucket_name
    lb_prefix               = var.lb_prefix
    account_id              = var.account_id
  }
}