terraform {
  backend "s3" {
    bucket = "dolap-application-service-${ENVIRONMENT}"
    key = "terraform.tfstate"
  }
}