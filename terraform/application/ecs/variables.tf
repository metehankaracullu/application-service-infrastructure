# - - - - - - - - - - - - - - -
# ECS SERVICE
# - - - - - - - - - - - - - - -
variable "service_name" {}
variable "ecs_cluster_name" {}
variable "desired_task_number" {}
variable "private_1a" {}
variable "private_1b" {}
variable "private_1c" {}
variable "lb_direct_container_port" {
  default = "8080"
}
variable "services" {
  description = "A list of Services"
  type        = list(string)
  default     = ["api", "batch"]
}

# - - - - - - - - - - - - - - - -
# ECS TASK DEFINITION MODULE
# - - - - - - - - - - - - - - - -
variable "container_port" {}
variable "task_cpu" {}
variable "task_memory" {}
variable "account_id" {}
variable "region" {}
variable "environment" {}
variable "config_server_host" {}

# - - - - - - - - - - - - - - - -
# SECURITY GROUP MODULE
# - - - - - - - - - - - - - - - -
variable "vpc_id" {}
variable "vpc_cidr" {}

# - - - - - - - - - - - - - - - -
# ALB TG
# - - - - - - - - - - - - - - - -
variable "tg-http-port" {
  default = 80
}
variable "health_check_path" {}
variable "lb_logs_bucket_name" {}
variable "lb_prefix" {}