resource "aws_ecs_service" "ecs_service" {
  count              = length(var.services)
  launch_type        = "FARGATE"
  name               = "${var.services[count.index]}-${var.environment}"
  task_definition    = element(module.task_definition.task_definition_name[0], count.index )
  cluster            = var.ecs_cluster_name
  desired_count      = var.desired_task_number
  network_configuration {
    security_groups  = [module.security_group.task_sg_id]
    subnets          = local.subnet_ids
    assign_public_ip = false
  }
  load_balancer {
    container_name   = var.services[count.index]
    container_port   = var.lb_direct_container_port
    target_group_arn = aws_lb_target_group.service_alb_target_group[count.index].arn
  }
  propagate_tags = "SERVICE"
  tags = merge(
    local.common_tags,
    map(
      "Name", "${var.services[count.index]} ECS Service"
    )
  )
  depends_on = [aws_lb_target_group.service_alb_target_group]
}

resource "aws_lb" "ecs_lb" {
  name                        = "${var.service_name}-${var.environment}"
  internal                    = true
  load_balancer_type          = "application"
  security_groups             = [
    module.security_group.lb_sg_id]
  subnets                     = local.subnet_ids

  access_logs {
    bucket  = aws_s3_bucket.application_service_load_balancer_logs.bucket
    prefix  = var.lb_prefix
    enabled = true
  }
  tags = merge(
    local.common_tags,
    map(
      "Name", "application Service Api LB"
    )
  )
}

resource "aws_lb_listener" "http_listener_application_lb" {
  count               = "1"
  load_balancer_arn   = aws_lb.ecs_lb.arn
  port                = "80"
  protocol            = "HTTP"

  default_action {
    type              = "forward"
    target_group_arn  = element(aws_lb_target_group.service_alb_target_group.*.arn, 0)
  }

  depends_on = [aws_lb_target_group.service_alb_target_group]
}

resource "aws_lb_listener_rule" "host_based_routing" {
  count        = length(var.services)
  listener_arn = element(aws_lb_listener.http_listener_application_lb.*.arn, 0)
  priority     = 1 + count.index

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.service_alb_target_group[count.index].arn
  }

  condition {
    path_pattern {
      values = ["/${var.services[count.index]}/*"]
    }
  }
  depends_on = [aws_lb_listener.http_listener_application_lb]
}

resource "aws_lb_target_group" "service_alb_target_group" {
  count                 = length(var.services)
  name                  = "${var.service_name}-${var.services[count.index]}-${var.environment}"
  port                  = var.tg-http-port
  protocol              = "HTTP"
  vpc_id                = var.vpc_id
  target_type           = "ip"
  deregistration_delay  = 10
  health_check {
    path                = "/${var.services[count.index]}${var.health_check_path}"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = 5
    interval            = 10
    healthy_threshold   = 2
    unhealthy_threshold = 10
  }
  tags = merge(
    local.common_tags,
    map(
      "Name", "application Service LB Target Group"
    )
  )
  depends_on = [aws_lb.ecs_lb]
}

resource "aws_s3_bucket" "application_service_load_balancer_logs" {
  bucket                  = var.lb_logs_bucket_name
  force_destroy = true

  tags = merge(
    local.common_tags,
    map(
      "Name", "application Service Load Balancer Logs"
    )
  )
}

resource "aws_cloudwatch_log_group" "ecs_log_group" {
  count             = length(var.services)
  name              = "${var.service_name}-${var.services[count.index]}-${var.environment}-LogGroup"
  retention_in_days = 3
  tags = merge(
    local.common_tags,
    map(
      "Name", "application Service ECS Log Group"
    )
  )
}

module "security_group" {
  source = "../security_groups"

  vpc_id = var.vpc_id
  vpc_cidr = var.vpc_cidr
  environment = var.environment
}

module "task_definition" {
  source = "../task_definition"

  container_port          = var.container_port
  task_cpu                = var.task_cpu
  task_memory             = var.task_memory
  service_name            = var.service_name
  account_id              = var.account_id
  region                  = var.region
  environment             = var.environment
  config_server_host      = var.config_server_host
  services                = var.services
}

locals {
  common_tags = {
    ManagedBy = "terraform"
    Team = "Team"
    Project = "application-service-infrastructure"
    Domain = "application"
    Component = "application-service-application"
    Stage = var.environment
  }
}

locals {
  subnet_ids = toset([
    var.private_1a,
    var.private_1b,
    var.private_1c,
  ])
}
