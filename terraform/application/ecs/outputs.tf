output "ecs_service_name" {
  value = [aws_ecs_service.ecs_service.*.name]
}
output "load_balancer_dns_name" {
  value = aws_lb.ecs_lb.dns_name
}
output "target_group_arn" {
  value = [aws_lb_target_group.service_alb_target_group.*.arn]
}
output "load_balancer_arn" {
  value = aws_lb.ecs_lb.arn
}
output "log_bucket_name" {
  value = aws_s3_bucket.application_service_load_balancer_logs.bucket
}