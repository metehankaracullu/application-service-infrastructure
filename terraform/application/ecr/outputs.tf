output "ecr_repository_url" {
  value = aws_ecr_repository.application-service-api.repository_url
}