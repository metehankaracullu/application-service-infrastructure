resource "aws_ecr_repository" "application-service-api" {
  name = "${var.ecr_repo_name}-${var.environment}"
  tags = merge(
  local.common_tags,
  map(
  "Name", "application Service repository"
  )
  )
}

locals {
  common_tags = {
    ManagedBy = "terraform"
    Team = "Team"
    Project = "application-service-infrastructure"
    Domain = "application"
    Component = "application-service-application"
    Stage = var.environment
  }
}