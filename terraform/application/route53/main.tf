resource "aws_route53_record" "application_service_record" {
  zone_id = var.hosted_zone_id
  name    = var.service_name
  type    = "CNAME"
  ttl     = "300"
  records = [var.lb_dns_name]
}