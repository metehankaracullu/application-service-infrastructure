# Configure AWS Credentials & Region
provider "aws" {
  region = var.region
  version = "~> 3.1.0"
}