resource "aws_ecs_cluster" "ecs_cluster" {
  name = "${var.ecs_cluster_name}-${var.environment}"

  setting {
    name = "containerInsights"
    value = "enabled"
  }

  tags = merge(
    local.common_tags,
    map(
      "Name", "application Service Cluster"
    )
  )
}

module "ecs" {
  source = "./ecs"

  task_cpu            = var.task_cpu
  task_memory         = var.task_memory
  environment         = var.environment
  private_1a          = var.private_1a
  private_1b          = var.private_1b
  private_1c          = var.private_1c
  region              = var.region
  vpc_cidr            = var.vpc_cidr
  vpc_id              = var.vpc_id
  service_name        = var.service_name
  ecs_cluster_name    = "${var.ecs_cluster_name}-${var.environment}"
  account_id          = var.account_id
  config_server_host  = var.config_server_host
  container_port      = var.container_port
  desired_task_number = var.desired_task_number
  health_check_path   = var.health_check_path
  lb_logs_bucket_name = "${var.lb_logs_bucket_name}-${var.environment}"
  lb_prefix           = var.lb_prefix
}

module "autoscaling" {
  source = "./autoscaling"
  environment         = var.environment
  service_name        = var.service_name
  ecs_cluster_name    = "${var.ecs_cluster_name}-${var.environment}"
  services_and_capacities = var.services_and_capacities
  depends_on = [module.ecs]
}

data "aws_elb_service_account" "main" {}

module "lb_log_bucket_policy" {
  source = "./s3_policy"

  log_bucket_name         = module.ecs.log_bucket_name
  elb_service_account_arn = data.aws_elb_service_account.main.arn
  lb_prefix               = var.lb_prefix
  account_id              = var.account_id
  providers = {
    aws                   = aws
  }
}

module "route53" {
  source = "./route53"

  hosted_zone_id                = var.hosted_zone_id
  lb_dns_name                   = module.ecs.load_balancer_dns_name
  service_name                  = var.service_name
  providers = {
    aws                   = aws
  }
}

locals {
  common_tags = {
    ManagedBy = "terraform"
    Team = "Team"
    Project = "application-service-infrastructure"
    Domain = "application"
    Component = "application-service-application"
    Stage = var.environment
  }
}