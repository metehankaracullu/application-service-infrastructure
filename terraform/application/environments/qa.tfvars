vpc_cidr = "11.0.0.0/16"
config_server_host = "config-server.qa"
task_cpu = 512
task_memory = 1024
desired_task_number = 1
private_1a = "subnet-xxx"
private_1b = "subnet-yyy"
private_1c = "subnet-zzz"
services_and_capacities = {
  api = {
    min = 1
    max = 1
    name = "api"
  },
  consumer = {
    min = 1
    max = 1
    name = "consumer"
  }
}