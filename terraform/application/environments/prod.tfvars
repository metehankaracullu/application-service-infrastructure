vpc_cidr = "10.0.0.0/16"
config_server_host = "config-server.prod"
task_cpu = 2048
task_memory = 4096
desired_task_number = 1
private_1a = "subnet-xxx"
private_1b = "subnet-yyy"
private_1c = "subnet-zzz"
services_and_capacities = {
  api = {
    min = 1
    max = 4
    name = "api"
  },
  consumer = {
    min = 1
    max = 4
    name = "consumer"
  }
}