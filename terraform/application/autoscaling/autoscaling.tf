resource "aws_appautoscaling_target" "ecs_target" {
  for_each           = var.services_and_capacities
  max_capacity       = each.value["max"]
  min_capacity       = each.value["min"]
  resource_id        = "service/${var.ecs_cluster_name}/${each.value["name"]}-${var.environment}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

# Automatically scale capacity up by one
resource "aws_appautoscaling_policy" "up" {
  for_each           = var.services_and_capacities
  name               = "${each.value["name"]}-scale-up-policy"
  service_namespace  = "ecs"
  policy_type        = "TargetTrackingScaling"
  resource_id        = "service/${var.ecs_cluster_name}/${each.value["name"]}-${var.environment}"
  scalable_dimension = "ecs:service:DesiredCount"
  target_tracking_scaling_policy_configuration {
    target_value       = 50
    disable_scale_in   = true
    scale_in_cooldown  = 300
    scale_out_cooldown = 200
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
  }
  depends_on = [aws_appautoscaling_target.ecs_target]
}

# Automatically scale capacity down by one
resource "aws_appautoscaling_policy" "down" {
  for_each           = var.services_and_capacities
  name               = "${each.value["name"]}-scale-down-policy"
  service_namespace  = "ecs"
  resource_id        = "service/${var.ecs_cluster_name}/${each.value["name"]}-${var.environment}"
  scalable_dimension = "ecs:service:DesiredCount"
  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 300
    metric_aggregation_type = "Maximum"
    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = -1
    }
  }
  depends_on = [aws_appautoscaling_target.ecs_target]
}

# CloudWatch alarm that triggers the autoscaling down policy
resource "aws_cloudwatch_metric_alarm" "service_cpu_low" {
  for_each            = var.services_and_capacities
  alarm_name          = "${each.value["name"]}_cpu_utilization_low"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "300"
  statistic           = "Average"
  threshold           = "15"
  dimensions = {
    ClusterName = var.ecs_cluster_name
    ServiceName = "${each.value["name"]}-${var.environment}"
  }
  tags = merge(
    local.common_tags,
    map(
      "Name", "application Service CPU low alarm"
    )
  )
  alarm_actions = [aws_appautoscaling_policy.down[each.key].arn]
}

locals {
  common_tags = {
    ManagedBy = "terraform"
    Team = "Team"
    Project = "application-service-infrastructure"
    Domain = "application"
    Component = "application-service-application"
    Stage = var.environment
  }
}