#!/bin/bash
region=$AWS_REGION
bucket="dolap-application-service-$ENVIRONMENT"

echo "REGION : ${region}"
echo "BUCKET : ${bucket}"

aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY

bucket_check=$(aws s3api head-bucket --bucket $bucket 2>&1)

if [[ -z $bucket_check ]]; then
  echo "S3 Bucket ${bucket} already exists"
else
  echo "Creating s3 Bucket ${bucket}"
  aws s3api create-bucket --bucket ${bucket} --region ${region} --create-bucket-configuration LocationConstraint=${region}
  sleep 10 # Wait for creating the bucket
  aws s3api put-public-access-block --bucket ${bucket} --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"
  aws s3api put-bucket-encryption --bucket ${bucket} --server-side-encryption-configuration '{"Rules": [{"ApplyServerSideEncryptionByDefault": {"SSEAlgorithm": "AES256"}}]}'
fi
