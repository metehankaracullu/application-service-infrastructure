#!/bin/bash
BACKEND_BUCKET_NAME="dolap-application-service-$ENVIRONMENT"
STATE_FILE="terraform-service.tfstate"
cd  terraform/application || exit

if [ "$1" == "plan-infrastructure" ];then
  terraform init --backend-config "bucket=$BACKEND_BUCKET_NAME" --backend-config "key=$STATE_FILE" --backend-config "region=$AWS_REGION"
  terraform plan -var-file="environments/${ENVIRONMENT}.tfvars"
  echo Plan infrastructure ended!

elif [ "$1" == "deploy-infrastructure" ];then
  terraform init --backend-config "bucket=$BACKEND_BUCKET_NAME" --backend-config "key=$STATE_FILE" --backend-config "region=$AWS_REGION"
  terraform apply -parallelism=1 -var-file="environments/${ENVIRONMENT}.tfvars" -auto-approve=true
  echo Deploy infrastructure ended!

elif [ "$1" == "destroy" ];then
  terraform init --backend-config "bucket=$BACKEND_BUCKET_NAME" --backend-config "key=$STATE_FILE" --backend-config "region=$AWS_REGION"
  terraform destroy -var-file="environments/${ENVIRONMENT}.tfvars" --force -auto-approve=true
	echo Destroy infrastructure ended!
fi
